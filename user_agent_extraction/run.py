""" Executable script for "Machine Learning based model for Extracting Browser Family from User Agents". """
import numpy as np
import re
import random
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import tree
import argparse


def processFiles(filename):
    """
    Read file, extract meaningful words in user agent and get the groundtruth of browser family.
    Inputs:
    - x: path of data file
    Returns:
    - content_list: list of meaningful words for N user agents
    - y: list of browser family index for N user agents
    """
    i = 0
    content_list = [] 
    y = []
    with open(filename,'r') as f:
        for line in f:
            line = line.strip().split('\t')
            ## ith category number
            y_i = FAMILY_DICT[line[1]]
            y.append(y_i)
            ## ith content_list
            content_list_i = []
            lst = re.findall('\[[^\]]*\]|\([^\)]*\)|\S+',line[0])
            # special processing for the first element
            content_list_i.append(lst[0].strip())
            # special processing for the second element
            ele2 = lst[1][1:-2]
            lst2 = re.findall('\;|\S+', ele2)
            for item in lst2:
                # remove part has no a-zA-Z
                if re.sub("[^a-zA-Z]", "", item) != "":
                    content_list_i.append((item.strip(";")).strip())
            # other part
            if len(lst) > 2:
                for item in lst[2:]:
                    item = (item.strip("[")).strip("]")
                    # remove part has no a-zA-Z
                    if re.sub("[^a-zA-Z]", "", item) != "":
                        content_list_i.append(item.strip())
            content_list.append("/".join(content_list_i))
            i += 1
    return content_list, y

def tokenize(text):
    """
    Helper function for tfidfVectorizer.
    """
    return text.split('/')

def tfidfVectorizer(content_list):
    """
    Convert content_list to TF-IDF features. 
    Inputs:
    - content_list: list of meaningful words for N user agents
    Returns:
    - X: a numpy array for user agents features, of shape (N, MAX_FEATURES)
    - vectorizer: model for tfidfVectorizer, for further use of test data conversion
    - vocab: list of selected features
    """
    vectorizer = TfidfVectorizer(analyzer = "word",  \
                                tokenizer = tokenize,   \
                                preprocessor = None, \
                                stop_words = None,   \
                                lowercase = False,\
                                max_features = MAX_FEATURES)
    X = vectorizer.fit_transform(content_list)
    X = X.toarray()
    vocab = vectorizer.get_feature_names()
    return X, vectorizer, vocab
    

def writePrediction(filename_test, filename_predict, y):
    """
    Write the prediction result into file.
    Inputs:
    - filename_test: path of test data file
    - filename_predict: path of prediction result file (output file)
    - y: list of predicted browser family index for N user agents
    """
    with open(filename_test,'r') as f_test:
        with open(filename_predict,'w') as f_predict:
            i = 0
            for line in f_test:
                pred_uafamily = FAMILY_DICT2[y[i]]
                f_predict.write(line.strip() + '\t' + pred_uafamily + '\n')
                i += 1


def main():
    """Main function"""
    # parse argument
    parser = argparse.ArgumentParser()
    parser.add_argument('--training', default='', type=str)
    parser.add_argument('--test', default='', type=str)
    parser.add_argument('--prediction_results', default='', type=str)
    args = parser.parse_args()
    train_file = args.training
    test_file = args.test
    predict_file = args.prediction_results
    
    # process files
    content_list_train, y_train = processFiles(train_file)
    X_train, vectorizer, vocab = tfidfVectorizer(content_list_train)
    print("X_train shape {}, y_train len {}".format(X_train.shape, len(y_train)))
    #print(vocab)
    
    # shuffle training data
    new_order = np.arange(X_train.shape[0])
    np.random.shuffle(new_order)
    X_train_shuffle = X_train[new_order]
    # array to np
    y_train = np.array(y_train)
    y_train_shuffle = y_train[new_order]
    # np to array
    y_train_shuffle = [int(x) for x in y_train_shuffle.tolist()]

    # decision tree
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X_train, y_train)
    print("training score: {}".format(clf.score(X_train_shuffle, y_train_shuffle)))

    # for prediction
    # process files
    content_list_test, y_test = processFiles(test_file)
    X_test = vectorizer.fit_transform(content_list_test)
    X_test = X_test.toarray()
    print("X_test shape {}, y_test len {}".format(X_test.shape, len(y_test)))
    # predict 
    y_predict = clf.predict(X_test)
    print("test score: {}".format(clf.score(X_test, y_test)))

    # write prediction results
    writePrediction(test_file, predict_file, y_predict)

if __name__ == '__main__':
    
    # hyper-parameters
    MAX_FEATURES = 800 # selected by validation 

    # CONSTANT, family dictionary, key - user agent family name, str; value - category number, int.
    FAMILY_DICT = {'Chrome': 0, 'Chrome Mobile': 1, 'Chrome Mobile iOS': 2, 'Safari': 3, 'Mobile Safari': 4,\
     'IE': 5, 'IE Mobile': 6, 'Edge': 7, 'Edge Mobile': 8, 'Firefox': 9, 'Firefox Mobile': 10, 'Firefox iOS': 11,\
     'Opera': 12, 'Opera Mobile': 13, 'Opera Mini': 14, 'UC Browser': 15, 'Sogou Explorer': 16, 'QQ Browser': 17, \
     'QQ Browser Mobile': 18, 'Maxthon': 19, 'AOL': 20, 'Facebook': 21, 'AppleMail': 22, 'Puffin': 23, 'Android': 24, \
     'YandexSearch': 25, 'BlackBerry WebKit': 26, 'Amazon Silk': 27}

    # family dictionary, key - category number, str; value - user agent family name, int.
    FAMILY_DICT2 = {0:'Chrome', 1:'Chrome Mobile', 2:'Chrome Mobile iOS', 3:'Safari', 4:'Mobile Safari',\
     5:'IE', 6:'IE Mobile', 7:'Edge', 8:'Edge Mobile', 9:'Firefox', 10:'Firefox Mobile', 11:'Firefox iOS',\
     12:'Opera', 13:'Opera Mobile', 14:'Opera Mini', 15:'UC Browser', 16:'Sogou Explorer', 17:'QQ Browser', \
     18:'QQ Browser Mobile', 19:'Maxthon', 20:'AOL', 21:'Facebook', 22:'AppleMail', 23:'Puffin', 24:'Android', \
     25:'YandexSearch', 26:'BlackBerry WebKit', 27:'Amazon Silk'}

    #Main function
    main()

