# coding: utf-8

import collections 
import scipy.sparse as sp
import numpy as np
import re

def load_federalist_corpus(filename):
    """ Load the federalist papers as a tokenized list of strings, one for each eassay"""
    with open(filename, "rt") as f:
        data = f.read()
    papers = data.split("FEDERALIST")
    
    # all start with "To the people of the State of New York:" (sometimes . instead of :)
    # all end with PUBLIUS (or no end at all)
    locations = [(i,[-1] + [m.end()+1 for m in re.finditer(r"of the State of New York", p)],
                 [-1] + [m.start() for m in re.finditer(r"PUBLIUS", p)]) for i,p in enumerate(papers)]
    papers_content = [papers[i][max(loc[1]):max(loc[2])] for i,loc in enumerate(locations)]

    # discard entries that are not actually a paper
    papers_content = [p for p in papers_content if len(p) > 0]

    # replace all whitespace with a single space
    papers_content = [re.sub(r"\s+", " ", p).lower() for p in papers_content]

    # add spaces before all punctuation, so they are separate tokens
    punctuation = set(re.findall(r"[^\w\s]+", " ".join(papers_content))) - {"-","'"}
    for c in punctuation:
        papers_content = [p.replace(c, " "+c+" ") for p in papers_content]
    papers_content = [re.sub(r"\s+", " ", p).lower().strip() for p in papers_content]
    
    authors = [tuple(re.findall("MADISON|JAY|HAMILTON", a)) for a in papers]
    authors = [a for a in authors if len(a) > 0]
    
    numbers = [re.search(r"No\. \d+", p).group(0) for p in papers if re.search(r"No\. \d+", p)]
    
    return papers_content, authors, numbers
    
    

def tfidf(docs):
    """
    Create TFIDF matrix.  This function creates a TFIDF matrix from the
    docs input.

    Args:
        docs: list of strings, where each string represents a space-separated
              document
    
    Returns: tuple: (tfidf, all_words)
        tfidf: sparse matrix (in any scipy sparse format) of size (# docs) x
               (# total unique words), where i,j entry is TFIDF score for 
               document i and term j
        all_words: list of strings, where the ith element indicates the word
                   that corresponds to the ith column in the TFIDF matrix
    """
    all_words = []
    vocab = {} # key: word, value: index
    df = {} # key: word, value: doc frequency
    regex = re.compile("\s+")
    idx = 0
    for doc in docs:
        items = re.split(regex, doc)
        for item in set(items):
            if item != "":
                if item not in vocab:
                    vocab[item] = idx
                    all_words.append(item)
                    idx += 1
                    df[item] = 1
                else:
                    df[item] += 1

    m = len(docs)  # number of documents
    tfidf = [] # term frequency
    for doc in docs:
        line = {}
        items = re.split(regex, doc)
        tf = dict(collections.Counter(items))
        for word in tf:
            if word == "":
                continue
            score = float(tf[word]) * np.log(m/df[word])
            if score > 0:
                word_idx = vocab[word]
                line[word_idx] = score
        tfidf.append(line)
    row_lst = []
    col_lst = []
    data_lst = []
    for row in range(m):
        for col, score in tfidf[row].items():
            row_lst.append(row)
            col_lst.append(col)
            data_lst.append(score)
    tfidf = sp.csr_matrix((data_lst, (row_lst, col_lst)), shape = (m, len(all_words)))
    return (tfidf, all_words)


def cosine_similarity(X):
    """
    Return a matrix of cosine similarities.
    
    Args:
        X: sparse matrix of TFIDF scores or term frequencies
    
    Returns:
        M: dense numpy array of all pairwise cosine similarities.  That is, the 
           entry M[i,j], should correspond to the cosine similarity between the 
           ith and jth rows of X.
    """
    M = X.dot(X.T).todense()
    n = len(M)
    norm = np.zeros((n,1))
    for i in range(n):
        norm[i] = 1 / M.item(i, i) ** 0.5
    M = np.multiply(M, norm.dot(norm.T))
    return M



class LanguageModel:
    flag = 0
    regex = re.compile("\s+")
    def __init__(self, docs, n):
        """
        Initialize an n-gram language model.
        
        Args:
            docs: list of strings, where each string represents a space-separated
                  document
            n: integer, degree of n-gram model
        """
        self.n = n
        self.tuples = {}
        self.vocab = set()
        
        for doc in docs:
            items = re.split(LanguageModel.regex, doc)
            self.vocab.update(items) 
            for i in range(len(items) + 1 - n):
                tp = tuple(items[i:i+n-1])
                if tp not in self.tuples:
                    self.tuples[tp] = {}
                    self.tuples[tp][LanguageModel.flag] = 1
                else:
                    self.tuples[tp][LanguageModel.flag] += 1
                if items[i+n-1] not in self.tuples[tp]:
                    self.tuples[tp][items[i+n-1]] = 1
                else:
                    self.tuples[tp][items[i+n-1]] += 1
                    
                    
    def perplexity(self, text, alpha=1e-3):
        """
        Evaluate perplexity of model on some text.
        
        Args:
            text: string containing space-separated words, on which to compute
            alpha: constant to use in Laplace smoothing
            
        Note: for the purposes of smoothing, the dictionary size (i.e, the D term)
        should be equal to the total number of unique words used to build the model
        _and_ in the input text to this function.
            
        Returns: perplexity
            perplexity: floating point value, perplexity of the text as evaluted
                        under the model.
        """
        items = re.split(LanguageModel.regex, text)
        sum_lgp = 0
        total_num = len(self.vocab.union(set(items)))
        times = len(items)-self.n+1
        for i in range(times):
            tp = tuple(items[i:i+self.n-1])
            last = items[i+self.n-1]
            c1 = 0
            c2 = 0
            if tp in self.tuples:
                c1 = self.tuples[tp][LanguageModel.flag]
                if last in self.tuples[tp]:
                    c2 = self.tuples[tp][last]
            lgp = np.log2(c2 + alpha) - np.log2(c1 + alpha * total_num)
            sum_lgp += lgp
        return np.power(2, -sum_lgp/times)
    
    def sample(self, k):
        """
        Generate a random sample of k words.
        
        Args:
            k: integer, indicating the number of words to sample
            
        Returns: text
            text: string of words generated from the model.
        """
        res = []
        tp_lst = self.gen_start()
        while (len(res)  < k):
            tp = tuple(tp_lst)
            if tp in self.tuples:
                rand_idx = np.random.randint(0, len(self.tuples[tp]))
                next_word = list(self.tuples[tp].keys())[rand_idx]
                if next_word == LanguageModel.flag:
                    next_word = list(self.tuples[tp].keys())[rand_idx+1]
                res.append(tp_lst[0])
                tp_lst = tp_lst[1:]
                tp_lst.append(next_word)
            else:
                tp_lst = self.gen_start()
        return " ".join(res)
    
    def gen_start(self):
        rand_idx = np.random.randint(0, len(self.tuples))
        idx = 0
        for tp in self.tuples:
            if idx == rand_idx:
                return list(tp)
            idx += 1
        
    
    
def main():
    hamilton = []
    madison = []
    jay = []
    unknown = []

    idx = -1
    for paper, author in zip(papers, authors):
        idx += 1
        if len(author) > 1:
            unknown.append(paper)
            continue
        if 'HAMILTON' in author:
            hamilton.append(paper)
        if 'MADISON' in author:
            madison.append(paper)
        if 'JAY' in author:
            jay.append(paper)

    l_hamilton = LanguageModel(hamilton, 3)
    l_madison = LanguageModel(madison, 3)
    l_jay = LanguageModel(jay, 3)
    print(l_hamilton.perplexity(papers[0]))

    cnt = 0
    perp_hamilton = 0
    perp_madison = 0
    perp_jay = 0
    for doc in unknown:
        cnt += 1
        perp_hamilton += l_hamilton.perplexity(doc)
        perp_madison += l_madison.perplexity(doc)
        perp_jay += l_jay.perplexity(doc)
    perp_hamilton /= cnt
    perp_madison /= cnt
    perp_jay /= cnt
    print(perp_hamilton)
    print(perp_madison)
    print(perp_jay)

    text = l_hamilton.sample(100)
    print("perplexity of generated text")
    print(l_hamilton.perplexity(text))

if __name__ == '__main__':
    main()
