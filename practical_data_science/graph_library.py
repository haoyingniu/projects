# coding: utf-8

import numpy as np
import scipy.sparse as sp
from heapdict import heapdict



class Graph:
    def __init__(self):
        """ Initialize with an empty edge dictionary. """
        self.edges = {}
    
    def add_edges(self, edges_list):
        """ Add a list of edges to the network. Use 1.0 to indiciate the presence of an edge. 
        
        Args:
            edges_list: list of (a,b) tuples, where a->b is an edge to add
            
        """
        
        for tp in edges_list:
            if tp[0] not in self.edges:
                self.edges[tp[0]] = {tp[1]:1}
            else:
                self.edges[tp[0]][tp[1]] = 1
            if tp[1] not in self.edges:
                self.edges[tp[1]] = {}

        
    def shortest_path(self, source):
        """ Compute the single-source shorting path.
        
        This function uses Djikstra's algorithm to compute the distance from 
        source to all other nodes in the network.
        
        Args:
            source: node index for the source
            
        Returns: tuple: dist, path
            dist: dictionary of node:distance values for each node in the graph, 
                  where distance denotes the shortest path distance from source
            path: dictionary of node:prev_node values, where prev_node indicates
                  the previous node on the path from source to node
                  
        
            d = heapdict({"a":5, "b":6})
            d["b"] = 4
            d.popitem() # -> ("b", 4)
        """
        frontier = heapdict()
        dist = {}
        path = {}
        unvisit = set(self.edges.keys())
        for key in unvisit:
            path[key] = None
            dist[key] = float("inf")
        node_tuple = (source, 0)
        dist[source] = 0
        unvisit.remove(source)
        while True:
            for node in self.edges[node_tuple[0]].keys():
                if node in unvisit:
                    unvisit.remove(node)
                    frontier[node] = node_tuple[1] + 1
                    path[node] = node_tuple[0]
                elif (node in frontier) and (node_tuple[1] + 1 < frontier[node]):
                    frontier[node] = node_tuple[1] + 1
                    path[node] = node_tuple[0]
            if (len(frontier) == 0):
                break
            else:
                node_tuple = frontier.popitem()
                dist[node_tuple[0]] = node_tuple[1]
        return dist, path
        
    
        
    def adjacency_matrix(self):
        """ Compute an adjacency matrix form of the graph.  
        
        Returns: tuple (A, nodes)
            A: a sparse matrix in COO form that represents the adjaency matrix
               for the graph (i.e., A[j,i] = 1 iff there is an edge i->j)
               NOTE: be sure you have this ordering correct!
            nodes: a list of nodes indicating the node key corresponding to each
                   index of the A matrix
        """
        nodes = sorted(list(self.edges.keys()))
        node_map = {}
        node_num = len(nodes)
        for i in range(node_num):
            node_map[nodes[i]] = i
        # add egde information
        row = [] # j
        col = [] # i
        for node_i in self.edges:
            node_js = self.edges[node_i]
            for node_j in node_js:
                col.append(node_map[node_i])
                row.append(node_map[node_j])
        data = np.ones(len(row), dtype=np.int8)
        row = np.array(row)
        col = np.array(col)
        A = sp.coo_matrix((data, (row,col)), shape=(node_num, node_num))
        return A, nodes
    
    def pagerank(self, d=0.85, iters=100):
        """ Compute the PageRank score for each node in the network.
        
        Compute PageRank scores using the power method.
        
        Args:
            d: 1 - random restart factor
            iters: maximum number of iterations of power method
            
        Returns: dict ranks
            ranks: a dictionary of node:importance score, for each node in the
                   network (larger score means higher rank)
        
        """
        n = len(self.edges)
        A, nodes = self.adjacency_matrix()
        # normalize
        sum_arr = np.array(A.sum(0), dtype=float)
        rows, cols = A.nonzero()
        A.data = A.data/sum_arr[0,cols]
        A = A.tocsr()
        
        # decomposition
        p = np.ones((1,n))/float(n)
        nonzeros = (sum_arr == 0)
        q = (nonzeros/n).reshape((1,n))

        # iterations
        x = np.ones((n,1))
        for i in range(iters):
            to_add = (q*d + p*(1-d)).dot(x)
            x = A.dot(d*x) + np.repeat(to_add, n, axis = 0)
        x /= np.sum(x)
        
        ranks = {}
        for i in range(n):
            ranks[nodes[i]] = x[i][0]
        return ranks  
        